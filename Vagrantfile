# -*- mode: ruby -*-
# vi: set ft=ruby :
require "yaml"

env = ENV.fetch('HD_ENV', 'local')      # { local, dev }

boxes = {
 'local' => 'centos/7',
 'lxc' => 'frensjan/centos-7-64-lxc'
}

Vagrant.configure(2) do |config|
  config.vm.box_check_update = false

  config.vm.provider "virtualbox" do |v, override|
    override.vm.box = boxes['local']
  end

  config.vm.provider "lxc" do |l, override|
    override.vm.box = boxes['lxc']
  end

  if File.exist?("config/#{env}/ips.yaml")
    ips = YAML.load_file("config/#{env}/ips.yaml")
  else
    ips = nil
  end

  config.vm.define 'namenode' do |namenode|
    namenode.vm.provider "virtualbox" do |vm, override|
      override.vm.network 'private_network', ip: ips['namenode'] if ips
      override.vm.synced_folder '.', '/home/vagrant/sync', disabled: true
      override.vm.synced_folder '.', '/vagrant', type: 'rsync', rsync__exclude: '.git/'
    end
  end

  config.vm.define 'datanode1' do |datanode1|
    datanode1.vm.provider "virtualbox" do |vm, override|
      override.vm.network 'private_network', ip: ips['datanode1'] if ips
      override.vm.synced_folder '.', '/home/vagrant/sync', disabled: true
      override.vm.synced_folder '.', '/vagrant', type: 'rsync', rsync__exclude: '.git/'
    end
  end

  config.vm.define 'datanode2' do |datanode2|
    datanode2.vm.provider "virtualbox" do |vm, override|
      override.vm.network 'private_network', ip: ips['datanode2'] if ips
      override.vm.synced_folder '.', '/home/vagrant/sync', disabled: true
      override.vm.synced_folder '.', '/vagrant', type: 'rsync', rsync__exclude: '.git/'
    end
  end

  config.vm.define 'hue' do |hue|
    hue.vm.provider "virtualbox" do |vm, override|
      override.vm.network 'private_network', ip: ips['hue'] if ips
      override.vm.synced_folder '.', '/home/vagrant/sync', disabled: true
      override.vm.synced_folder '.', '/vagrant', type: 'rsync', rsync__exclude: '.git/'
    end
  end

  config.vm.provider "lxc" do |l, override|
    $script = <<-SCRIPT
    cp /vagrant/files/vagrant_ssh_conf /home/vagrant/.ssh/config
    chown -R vagrant: /home/vagrant/.ssh/config
    SCRIPT
    override.vm.provision "shell", inline: $script
  end

  config.vm.provision "shell", inline: 'yum install -y wget'
end
