import os


DATABASE = {
    'server': os.getenv('HADOUKEN_DB_SERVER', ''),
    'port': os.getenv('HADOUKEN_DB_PORT', ''),
    'name': os.getenv('HADOUKEN_DB_NAME', ''),
    'username': os.getenv('HADOUKEN_DB_USER', ''),
    'password': os.getenv('HADOUKEN_DB_PASSWORD', '')
}
