from os import system
import sys

from pyspark.sql import SparkSession
from pyspark.sql import Row

import database
import csv


# TODO: Extract to Oozie attributes
OUT_FILE='out.csv'
HDFS_DIR='/tmp/fetch/'
HDFS_USER='admin'

# TODO insert this on hive-site.xml in the property spark.sql.warehouse.dir
warehouse_location = 'hdfs://namenode:54310/user/hive/warehouse'

spark = SparkSession \
    .builder \
    .appName("Python Spark SQL Hive integration example") \
    .config("spark.sql.warehouse.dir", warehouse_location) \
    .enableHiveSupport() \
    .getOrCreate()


df = spark.sql("SELECT max(trb_data) as max_data from transacoesbalancas")
rows = df.collect()
max_date = rows[0].max_data if (len(rows) > 0) else None
max_date = '2016-07-12 16:42:10'

# TODO: SQL injection alert: sanitize user input!!
table_name = sys.argv[1]
date_col = sys.argv[2]
columns = ", ".join(map(lambda c: '[%s]' % c.strip(), sys.argv[3].split(',')))

query = 'SELECT %s FROM [dbo].[%s]' % (columns, table_name)
filter = " WHERE [%s] > Convert(datetime, '%s')" % (date_col, max_date)
query += filter if (max_date != None) else ''

conn = database.connect()
cursor = conn.cursor()
cursor.execute(query)

with open(OUT_FILE, 'wb') as csv_file:
    writer = csv.writer(csv_file, delimiter=';')
    for row in cursor.fetchall():
        writer.writerow(row)


# TODO: move to run_fetch.sh?
system("hadoop fs -put -f %s %s" % (OUT_FILE, HDFS_DIR))
system("hadoop fs -chown %s: %s%s" % (HDFS_USER, HDFS_DIR, OUT_FILE))
