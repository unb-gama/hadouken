import pyodbc
from conf import DATABASE


def connect():
    return pyodbc.connect('Driver={ODBC Driver 13 for SQL Server}' +
                          ';Server=tcp:' + DATABASE['server'] +
                          ',' + DATABASE['port'] +
                          ';Database=' + DATABASE['name'] +
                          ';Uid=' + DATABASE['username'] +
                          ';Pwd=' + DATABASE['password'])
