require 'yaml'
require 'chake'
require 'erb'

begin
  load 'local.rake'
rescue LoadError
  # nothing
end

$HD_ENV = ENV.fetch('HD_ENV', 'local')

$CONVERGE_OPTS = {
  reinstall: ENV.fetch('REINSTALL', false)
}

config_file = "config/#{$HD_ENV}/%s"
ssh_config_file = config_file % "ssh_config"
ips_file = config_file % "ips.yaml"
tools_releases_file = config_file % "tools_releases.yaml"

@USER = 'hadoop'

ENV['CHAKE_TMPDIR'] = "tmp/chake.#{$HD_ENV}"
ENV['CHAKE_SSH_CONFIG'] = ssh_config_file

chake_rsync_options = ENV['CHAKE_RSYNC_OPTIONS'].to_s.clone
chake_rsync_options += ' --exclude backups'
chake_rsync_options += ' --exclude src'
ENV['CHAKE_RSYNC_OPTIONS'] = chake_rsync_options

puts ">>> Converging in #{$HD_ENV} environment..."

if $HD_ENV == 'lxc'
  system("mkdir -p config/lxc; sudo lxc-ls -f -F name,ipv4 | "\
         "sed -e '/^hadouken/ !d; s/hadouken_//; s/_[0-9_]*/:"\
         "/ ' > #{ips_file}.new; cp config/dev/tools_releases"\
         ".yaml config/lxc/")
  begin
    ips = YAML.load_file("#{ips_file}.new")
    tools_releases = YAML.load_file(tools_releases_file)
    raise ArgumentError, "Error reading ips file" unless ips.is_a?(Hash)
    raise ArgumentError, "Error reading tools_releases file" unless tools_releases.is_a?(Hash)
    FileUtils.mv ips_file + '.new', ips_file
  rescue Exception => ex
    puts ex.message
    puts
    puts "Q: did you boot the containers first?"
    exit
  end
end

task :preconfig do
  command = "sudo yum install -y wget rsync"
  $nodes.each do |node|
    puts "###############################################"
    puts "[#{node.hostname}]: EXECUTING #{command}"
    puts "This can take long"
    puts "###############################################"

    output = IO.popen("ssh -F #{ssh_config_file} #{node.hostname} #{command}")
    output.each_line do |line|
      printf "%s: %s\n", node.hostname, line.strip
    end
    output.close
    if $?
      status = $?.exitstatus
      if status != 0
        raise Exception.new(['FAILED with exit status %d' % status].join(': '))
      end
    end
  end
end

task :test do
  sh "HD_ENV=#{$HD_ENV} ./test/run_all"
end


if ['local', 'lxc'].include?($HD_ENV)
  @USER = 'vagrant'
end

task :clean do
  central_user = `echo $USER`.strip
  sh "rake run['sudo rm -rf /var/tmp/chef.#{central_user}']"
  sh "rm -rf tmp/*"
end

ips ||= YAML.load_file(ips_file)
tools_releases ||= YAML.load_file(tools_releases_file)

$nodes.each do |node|
  node.data['environment'] = $HD_ENV
  node.data['peers'] = ips
  node.data['ip'] = ips[node.hostname]
  node.data['user'] = @USER
  node.data['options'] = $CONVERGE_OPTS
  node.data['releases'] = tools_releases
end

if ['local', 'lxc'].include?($HD_ENV)
  template = ERB.new(File.read('ssh_config.erb'))
  File.open(ssh_config_file, 'w') do |f|
    f.write(template.result(binding))
  end
end

Dir.glob('tasks/*.rake').each { |f| load f }
