CREATE TABLE IF NOT EXISTS testTransactions (
  bal_codigo INT,
  itn_codigo INT,
  trb_data TIMESTAMP,
  trb_peso DECIMAL(10,3),
  trb_preco DECIMAL(10,2),
  trb_preco_total DECIMAL(10,2),
  trb_descritivo STRING
) row format delimited fields terminated by ';';

LOAD DATA INPATH '/tmp/fetch/out.csv' INTO TABLE testTransactions;
