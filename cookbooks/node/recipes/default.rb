OPTIONS = node['options']

HADOOP_USER = node['user']
HADOOP_VERSION = node['releases']['hadoop']
HADOOP_DIR = "/home/#{HADOOP_USER}/hadoop-#{HADOOP_VERSION}"
HADOOP_HOME = "/home/#{HADOOP_USER}"

BASHRC_PATH = File.join(HADOOP_HOME, ".bashrc")
BOOTSTRAP_FILE = File.join(HADOOP_HOME, ".node-bootstrap")

WAREHOUSE_DIR = '/user/hive/warehouse'

if OPTIONS["reinstall"]
  execute "rm -rf #{BOOTSTRAP_FILE}"

  execute 'cleaning hadoop due to REINSTALL' do
    command "rm -rf #{HADOOP_DIR}"
  end

  execute 'cleaning hdfs due to REINSTALL' do
    command "rm -rf /hadoop-data"
  end
end

execute "sudo chown -R #{HADOOP_USER}: /var/tmp/"

directory "/hadoop-data/hadoop/hdfs/namenode/" do
  owner HADOOP_USER
  group HADOOP_USER
  mode 0755
  recursive true
end

directory "/hadoop-data/hadoop/hdfs/datanode/" do
  owner HADOOP_USER
  group HADOOP_USER
  mode 0755
  recursive true
end

directory "/hadoop-data/hadoop/job-history/intermediate" do
  owner HADOOP_USER
  group HADOOP_USER
  mode 0755
  recursive true
end

directory "/hadoop-data/hadoop/job-history/done" do
  owner HADOOP_USER
  group HADOOP_USER
  mode 0755
  recursive true
end

directory "/hadoop-data" do
  owner HADOOP_USER
  group HADOOP_USER
  mode 0755
  recursive true
end

command = "ls #{HADOOP_DIR}"
output = `#{command}`
if output.eql? ""
  tar_extract "http://ftp.unicamp.br/pub/apache/hadoop/common/hadoop-#{HADOOP_VERSION}/hadoop-#{HADOOP_VERSION}.tar.gz" do
    target_dir "/home/#{HADOOP_USER}"
    creates HADOOP_DIR
  end
end

execute 'chmod:haddop:dir' do
  command "chown -R #{HADOOP_USER}: #{HADOOP_DIR} && chmod -R 744 #{HADOOP_DIR}"
  cwd "/home/#{HADOOP_USER}"
end

cookbook_file "/home/#{HADOOP_USER}/hadoop-#{HADOOP_VERSION}/etc/hadoop/hadoop-env.sh" do
  owner HADOOP_USER
  mode "0755"
end

cookbook_file "/home/#{HADOOP_USER}/hadoop-#{HADOOP_VERSION}/etc/hadoop/capacity-scheduler.xml" do
  owner HADOOP_USER
  mode "0755"
end

template "/home/#{HADOOP_USER}/hadoop-#{HADOOP_VERSION}/etc/hadoop/core-site.xml" do
  owner HADOOP_USER
  mode 0644
end

template "/home/#{HADOOP_USER}/hadoop-#{HADOOP_VERSION}/etc/hadoop/hdfs-site.xml" do
  owner HADOOP_USER
  mode 0644
end

template "/home/#{HADOOP_USER}/hadoop-#{HADOOP_VERSION}/etc/hadoop/yarn-site.xml" do
  owner HADOOP_USER
  mode 0644
end

template "/home/#{HADOOP_USER}/hadoop-#{HADOOP_VERSION}/etc/hadoop/mapred-site.xml" do
  owner HADOOP_USER
  mode 0644
end

template "/home/#{HADOOP_USER}/hadoop-#{HADOOP_VERSION}/etc/hadoop/slaves" do
  owner HADOOP_USER
  mode 0644
end

template "/etc/profile.d/hadoop.sh" do
  mode '0644'
  variables({
    HADOOP_HOME: HADOOP_DIR
  })
end

#execute "format hdfs filesystem" do
#  command "#{HADOOP_DIR}/bin/hdfs namenode -format"
#  user HADOOP_USER
#  not_if { ::File.exists?(BOOTSTRAP_FILE) }
#end
#
#execute "create warehouse dir on hdfs" do
#  command "#{HADOOP_DIR}/bin/hadoop fs -mkdir -p #{WAREHOUSE_DIR}"
#  user HADOOP_USER
#  not_if { ::File.exists?(BOOTSTRAP_FILE) }
#end
#
#execute "grant permissions to #{WAREHOUSE_DIR}" do
#  command "#{HADOOP_DIR}/bin/hadoop fs -chmod 777 #{WAREHOUSE_DIR}"
#  user HADOOP_USER
#  not_if { ::File.exists?(BOOTSTRAP_FILE) }
#end
#
#execute "grant permissions to /tmp on hdfs" do
#  command "#{HADOOP_DIR}/bin/hadoop fs -chmod 777 #{WAREHOUSE_DIR}"
#  user HADOOP_USER
#  not_if { ::File.exists?(BOOTSTRAP_FILE) }
#end

bash "create bootstrap file" do
  code <<-EOF
    touch #{BOOTSTRAP_FILE}
  EOF
  not_if { ::File.exists?(BOOTSTRAP_FILE) }
end
