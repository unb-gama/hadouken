(function() {

  var solr_api = 'http://localhost:8983/solr'
  var solr_endpoint = solr_api + '/quantil/query'

  var weightsChart = null
  var histogramChart = null

  var data = null


  document.addEventListener("DOMContentLoaded", function(event) {
    // TODO: get rows limit from API
    var query = '?q=*:*&group=true&group.field=transactions_with_quantile.itn_codigo&rows=27867'

    fetch(solr_endpoint + query).then(function(response) {
      response.json().then(function(json) {
        var groups = json.grouped['transactions_with_quantile.itn_codigo'].groups
        var products = groups.map(function(group) {
          return group.doclist.docs[0]['transactions_with_quantile.trb_descritivo']
        })
        generateProductsList(products)
      })
    })

    $("#products-select").on("change", function (e) {
      fetchProductData(this.value)
    });

    $('select').select2();

    $('#slider').slider({
      value: 30,
      min: 5,
      max: 50,
      step: 5,
      slide: function(event, ui) {
        $('#buckets-amount').val(ui.value);
        if (data != null) {
          generateHistogram(data)
        }
      }
    });
    $('#buckets-amount').val($('#slider').slider('value'));
  });



  function generateProductsList(products) {
    var select = document.getElementById('products-select');
    products.forEach(function(product) {
      var option = document.createElement("option");
      option.value = product.trim();
      option.text = product.trim();
      select.appendChild(option);
    })
  }


  function fetchProductData(productName) {
    var query = '?q=transactions_with_quantile.trb_descritivo:"' + productName + '"' +
                '&rows=27867&sort=transactions_with_quantile.dia+asc'
    fetch(solr_endpoint + query).then(function(response) {
      response.json().then(function(json) {
        data = dataFromCollection(json.response.docs)
        generateGraph(data)
        generateHistogram(data)
      })
    })
  }


  function generateGraph(data) {
    var chartData = {
      labels: data.labels,
      datasets: [{
          label: 'Peso Total',
          data: data.data,
          backgroundColor: data.colors
      },
      {
        type: 'line',
        label: 'Anomalias',
        backgroundColor: 'rgb(125,21,26)',
        fill: false,
        radius: 0.5,
        data: data.upperBounds,
      }]
    }
    weightsChart = drawChart(weightsChart, 'weights-chart', chartData)
  }


  function dataFromCollection(collection) {
    var data = {
      labels: [],
      data: [],
      colors: [],
      upperBounds: []
    }

    collection.forEach(function (el) {
      var totalWeight = el['transactions_with_quantile.peso_total']
      var upperBound = el['transactions_with_quantile.approxquantile']
      var date = el['transactions_with_quantile.dia']

      data.labels.push(moment(date).format('ll'))
      data.data.push(totalWeight)
      data.colors.push( totalWeight <= upperBound ? 'rgb(72,109,136)' : 'rgb(125,21,26)')
      data.upperBounds.push(upperBound)
    })
    return data
  }


  function generateHistogram(data) {
    var ts = $('#slider').slider('value')
    var histogram = d3.histogram().thresholds(ts)
    var buckets = histogram(data.data)

    var dataX = []
    var labels = []
    var colors = []
    buckets.forEach(function(bucket) {
      dataX.push(bucket.length)
      labels.push(bucket.x1)
      colors.push( bucket.x1 <= data.upperBounds[0] ? 'rgb(72,109,136)' : 'rgb(125,21,26)')
    })

    var chartData = {
      labels: labels,
      datasets: [{
          label: 'Total de Pesagens',
          data: dataX,
          backgroundColor: colors
      },
      {
        type: 'line',
        label: 'Anomalias',
        backgroundColor: 'rgb(125,21,26)',
        fill: false,
        radius: 0.5,
      }]
    }

    histogramChart = drawChart(histogramChart, 'histogram-chart', chartData)
  }


  function drawChart(chart, id, data) {
    var ctx = document.getElementById(id).getContext('2d');

    if (chart != null) {
      chart.destroy()
    }

    return new Chart(ctx, {
      type: 'bar',
      data: data,
      options: {
          responsive: false,
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:true
              }
            }],
            xAxes: [{
              ticks: {
                beginAtZero: false
              },
            }]
          }
      }
    });
  }
})()
