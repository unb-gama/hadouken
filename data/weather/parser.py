import sys
import csv
from datetime import datetime
import mysql.connector

# Create the database and the table before running the script:
# create database clima;
# create table measurements (date DATETIME, maxtemp FLOAT, mintemp FLOAT,
#                            humidity FLOAT, precipitation FLOAT);

connection = mysql.connector.connect(user='root', password='root',
                                     database='clima')
cursor = connection.cursor()

add_measurement = ("INSERT INTO measurements "
                   "(date, maxtemp, mintemp, precipitation, humidity) "
                   "VALUES (%s, %s, %s, %s, %s)")

def num(str):
    return float(str) if str else 0

with open(sys.argv[1], 'rb') as file:
    csv_reader = csv.DictReader(file, delimiter=';')
    for row in csv_reader:
        datetime = datetime.strptime('%s %s' % (row['Data'], row['Hora']),
                                     '%d/%m/%Y %H%M')
        cursor.execute(add_measurement, (datetime, num(row['TempMaxima']),
                                         num(row['TempMinima']),
                                         num(row['Precipitacao']),
                                         num(row['Umidade Relativa Media'])))

connection.commit()
cursor.close()
connection.close()
