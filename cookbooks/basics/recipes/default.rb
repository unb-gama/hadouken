package 'openssh-server'
package 'vim'
package 'java-1.8.0-openjdk-devel'
package 'make'
#libtidy is from epel
package 'epel-release'
package 'tar'

template '/etc/hosts' do
  owner 'root'
  group 'root'
  mode 0755
end
