import sys
from os.path import expanduser, join

from pyspark.sql import SparkSession
from pyspark.sql import Row

# TODO insert this on hive-site.xml in the property spark.sql.warehouse.dir
warehouse_location = 'hdfs://namenode:54310/user/hive/warehouse'

spark = SparkSession \
    .builder \
    .appName("Python Spark SQL Hive integration example") \
    .config("spark.sql.warehouse.dir", warehouse_location) \
    .enableHiveSupport() \
    .getOrCreate()

# spark is an existing SparkSession

results = spark.sql("SELECT \
                        concat(itn_codigo, cast(trb_data as DATE)) as index, \
                        itn_codigo, trb_descritivo, cast(trb_data AS DATE) as dia, \
                        ROUND(sum(trb_peso),2) as peso_total \
                    FROM transacoesbalancas \
                    WHERE itn_codigo IS NOT NULL \
                    GROUP BY \
                        concat(itn_codigo, cast(trb_data as DATE)), \
                        cast(trb_data AS DATE), \
                        trb_descritivo, \
                        itn_codigo" \
                   )

codes = results.rdd.map(lambda r: r.itn_codigo).distinct()
df2 = sqlContext.sql("select itn_codigo, percentile_approx(peso_total, 0.90) as approxQuantile from results group by itn_codigo")
df3 = results.join(df2, ['itn_codigo'])
df3.write.saveAsTable('transactions_with_quantile', format='orc', mode='overwrite')
