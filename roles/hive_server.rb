name 'hive_server'

description 'Hive server'

run_list *[
  'recipe[basics]',
  'recipe[hive]'
]
