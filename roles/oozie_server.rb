name 'oozie_server'

description 'Oozie server'

run_list *[
  'recipe[basics]',
  'recipe[oozie]'
]
