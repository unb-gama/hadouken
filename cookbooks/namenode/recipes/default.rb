template '/etc/init.d/dfs' do
  owner 'root'
  group 'root'
  mode 0755
end

template '/usr/lib/systemd/system/dfs.service' do
  owner 'root'
  group 'root'
  mode 0644
end

template '/etc/init.d/yarn' do
  owner 'root'
  group 'root'
  mode 0755
end

template '/usr/lib/systemd/system/yarn.service' do
  owner 'root'
  group 'root'
  mode 0644
end


template '/etc/init.d/hue' do
  owner 'root'
  group 'root'
  mode 0755
end

template '/usr/lib/systemd/system/hue.service' do
  owner 'root'
  group 'root'
  mode 0644
end

execute 'daemon-reload' do
  command 'systemctl daemon-reload'
  user 'root'
end

service 'dfs' do
  action [:restart, :enable]
end

service 'yarn' do
  action [:restart, :enable]
end

service 'hue' do
  action [:restart, :enable]
end
