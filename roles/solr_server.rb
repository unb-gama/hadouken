name 'solr_server'

description 'Solr server'

run_list *[
  'recipe[basics]',
  'recipe[solr]'
]
