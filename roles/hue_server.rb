name 'hue_server'

description 'Hue server'

run_list *[
  'recipe[basics]',
  'recipe[hue]'
]
