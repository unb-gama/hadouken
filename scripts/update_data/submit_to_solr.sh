hadoop fs -copyToLocal /user/hive/warehouse/result_transactions_by_hour .

for f in $(find result_transactions_by_hour/ -type f -maxdepth 1); do
    sed -i '1s/^/id,description,hour,count,total_weight,total_price\n/' $f
    curl 'http://hue:8983/solr/transactions_by_hour/update?commit=true' --data-binary @$f -H 'Content-type:application/csv'
done
