CREATE TABLE IF NOT EXISTS result_transactions_by_hour (
  id INT,
  description STRING,
  hour INT,
  count_transactions INT,
  total_weight DECIMAL(10,3),
  total_price DECIMAL(10,2)
) ROW FORMAT DELIMITED FIELDS TERMINATED BY ",";

INSERT INTO TABLE result_transactions_by_hour
SELECT
  itn_codigo AS codigo,
  trb_descritivo AS descricao,
  HOUR(cast(trb_data AS timestamp)) AS hora,
  COUNT(itn_codigo) AS count_pesagens,
  ROUND(sum(trb_peso),2) AS peso_total_gramas,
  ROUND(sum(trb_preco_total),2) AS preco_total_reais
FROM testtransactions WHERE itn_codigo IS NOT NULL
GROUP BY HOUR(cast(trb_data AS timestamp)), trb_descritivo, itn_codigo;
