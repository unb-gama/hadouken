name 'datanode_server'

description 'Datanode server'

run_list *[
  'recipe[basics]',
  'recipe[node]'
]
