# Chake cookbooks for big data analysis with Hadoop

This project contains the scripts to set up an environment
for big data using the Apache Hadoop set of tools.   
This is part of Toledo Project with partnership with the University of Brasília
and is under the proprerty of Toledo Enterprise.

# Overview

The infrastructure is made up of 4 machines: **central**, **namenode**, and two **datanodes**.   
They basically perform the following roles:


- **central:** This is from where we run the scripts in other machines. It can be a
simple machine with git and ruby installed.

- **namenode:** This is the main machine, responsible for maintain the data and triggering
job scripts in the data across the datanodes. It acts as a resource manager, using
the data nodes as an extension of memory and processing.

- **datanodes:** The datanodes, as already said above, are just extensions of resources for
the cluster. You can easyly add as many datanodes as you need. (Explained in the user guide)

# Development
  
You can setup a local environment using Vagrant. Our vagrant file currently supports
virtualbox and lxc providers. Follow the steps below to run your own instance.

### Dependencies

- Ubuntu/Debian system
- ruby >= 2.2.0
- chake >= 0.13
- vagrant

### Installation
    
- **1. Clone the repository:**

    ```
    $ git clone https://gitlab.com/unb-gama/hadouken.git
    ```
    
- **2. Install ruby and vagrant:**

    ```
    $ sudo apt-get install ruby vagrant
    ```

- **3. Choose a vagrant provider: LXC or VIRTUALBOX**

    **IF YOU CHOOSE LXC Provider (Linux Only)**

    Install lxc package and vagrant lxc plugin.
    
    ```
    $ sudo apt-get install lxc lxc-templates
    $ vagrant plugin install vagrant-lxc
    ```
    
    Enter the folder and start the virtual machines with **lxc provider**. Vagrant will
    download and set up the lxc files for CentOS from [AtlasHashicorp](https://atlas.hashicorp.com)
    
    ```
    $ cd hadouken/
    $ vagrant up --provider lxc
    ```
    
    Check the [vagrant-lxc github page](https://github.com/fgrehm/vagrant-lxc) for more details. 
    
    **IF YOU CHOOSE VIRTUALBOX Provider**
    
    ```
    $ sudo apt-get install virtualbox
    ```
    
    Enter the folder and start the virtual machines with **virtualbox provider**. Vagrant will
    download and set up the virtualbox files for CentOS from [AtlasHashicorp](https://atlas.hashicorp.com)

    ```
    $ cd hadouken/
    $ vagrant up --provider virtualbox
    ```
    
- **4. Set up the HD_ENV variable**

    The recipes can be run in different environments, such as production, development, local or whatever
    environment you have available.   
    
    For **development** porpouses this variable can assume two possible values:
    If you have choosen to install using vagrant with lxc, this variable must be set to lxc,    
    so it can build the correct settings described in the Rakefile. If you decided to use virtualbox provider, the default
    value assumed for that will be fine.   
    
    Every time you run `rake command` this variable will be taken into account.   
    If you don't set up a value for this variable it will defaults to **local**.   
    
    > **If you want to change the default vaule, open the file local.rake.example and read the instructions**
    
    ```
    $rake converge HD_ENV=lxc
    ```
    
- **5. Prepare the containers with the `preconfig` task (FIRST BOOT ONLY)**

    This step is supposed to be run only in the first time you boot up the   
    machines. **Remember to set the HD_ENV with the desired environment**.
    
    ```
    $ rake preconfig HD_ENV=lxc
    ```
    
- **6. Execute the cookbooks with `converge` task**

    The converge task will install and start all the services in the machines.
    All the changes made in the cookbooks can be applied through this command.
    
    ```
    $ rake converge HD_ENV=lxc
    ```
    
    The converge process will take several minutes, longer in the first place. It depends much on your network speed.   
    This task fetch all needed packages and set up each machine.   
    By the end of this process, you should have all the services up and running.

    Visit **localhost:8080** and check it out.
  
# User Guide

- [Architecture of the system]()
- [Tools]()
    - [Chake]()
    - [Hadopp]()
    - [Hue]()
    - [Hive]()
    - [Kylin]()
- [Setting up the production environment]()
- [Setting the database]()
- [Running jobs]()
- [Upgrading the tools]()

# FAQ

- How can I log in inside a machine?
- How can restore data to my clusters?

# References

- Chef documentation: https://docs.chef.io/
- Chake: https://github.com/terceiro/chake
- Hadoop:
- Hue:
- Kylin:
- Hive:
- Yarn:

# Authors

- @AlessandroCaetano
- @luanguimaraesla
- @gabrielssilva
- @TallysMartins
