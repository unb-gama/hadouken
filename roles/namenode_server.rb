name 'namenode_server'

description 'Namenode server'

run_list *[
  'recipe[basics]',
  'recipe[node]',
  'recipe[namenode]'
]
