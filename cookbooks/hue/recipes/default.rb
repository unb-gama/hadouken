require 'securerandom'
#TODO change tarball URL

HADOOP_USER = node['user']
HADOOP_HOME = "/home/#{HADOOP_USER}"
HUE_VERSION = node['releases']['hue']
HUE_DIR = "/usr/local/hue"
HUE_CONF_DIR = HUE_DIR + "/desktop/conf"
TARBALL_URL = "https://dl.dropboxusercontent.com/u/730827"\
                  "/hue/releases/#{HUE_VERSION}/hue-#{HUE_VERSION}.tgz"
HUE_DB_USER = 'hue'
HUE_DB_NAME = 'hue'
HUE_DB_PASS = 'hue'

OPTIONS = node['options']

secret_key = SecureRandom.hex

packages = %w(
  cyrus-sasl-devel cyrus-sasl-gssapi cyrus-sasl-md5 cyrus-sasl-plain
  ant gcc gcc-c++ krb5-devel mariadb mariadb-devel openssl-devel cyrus-sasl-devel
  cyrus-sasl-gssapi sqlite-devel libtidy libxml2-devel libxslt-devel libffi-devel
  openldap-devel python-devel python-simplejson python-setuptools maven git gmp-devel)

packages.each do |pkg_name|
  package pkg_name
end

# FIXME:
# The 'creates' attribute was supposed to check if the file already exists
# but it seems not to be working
command =  "ls #{HADOOP_HOME}/hue-#{HUE_VERSION}/Makefile"
output = `#{command}`
if output.eql? ""
  tar_extract TARBALL_URL do
    target_dir HADOOP_HOME
    user HADOOP_USER
    creates "#{HADOOP_HOME}/hue-#{HUE_VERSION}/Makefile"
  end
end

if OPTIONS["reinstall"]
  execute 'cleaning hue due to REINSTALL' do
    command "rm -r #{HUE_DIR}"
  end
end

execute 'install hue' do
  cwd "#{HADOOP_HOME}/hue-#{HUE_VERSION}"
  command 'make install'
  not_if "ls #{HUE_DIR}"
end

template "#{HUE_DIR}/desktop/conf/hue.ini" do
  source "hue.ini.erb"
  mode 0644
  owner HADOOP_USER
  group HADOOP_USER
  variables({
    server_user: HADOOP_USER,
    server_group: HADOOP_USER,
    default_user: HADOOP_USER,
    default_hdfs_superuser: HADOOP_USER,
    hive_server_host: "datanode1",
    hive_conf_dir: "/usr/local/hue/desktop/conf/",
    secret_key: secret_key
  })
end

cookbook_file "#{HUE_CONF_DIR}/hive-site.xml" do
  cookbook 'hive'
  owner node['user']
  group node['user']
  mode 0755
end

execute "change permissions for #{HUE_DIR}" do
  command "chown -R #{HADOOP_USER}: #{HUE_DIR}"
end

package 'postgresql-server'

execute "init postgresql cluster" do
  command "postgresql-setup initdb || true"
end

cookbook_file '/var/lib/pgsql/data/pg_hba.conf' do
  mode '0600'
  owner 'postgres'
  group 'postgres'
end

service 'postgresql' do
  action :start
end

execute "creating postgres' user for #{HUE_DB_USER}" do
  psql_command = "CREATE USER #{HUE_DB_USER} with createdb login password '#{HUE_DB_PASS}'"
  command "psql -U postgres -c #{ '"' + psql_command + '"' }"
  user 'postgres'
  ignore_failure true
end

execute "create database #{HUE_DB_NAME}" do
  command "psql -U postgres -c 'CREATE DATABASE #{HUE_DB_NAME} OWNER #{HUE_DB_USER}'"
  user 'postgres'
  ignore_failure true
end

package 'python-devel'
package 'postgresql-devel'

script "Start Virtual Enviroment" do
  cwd '/usr/local/hue/build/env/bin'
  interpreter "bash"
  code <<-EOH
  source activate
  pip install psycopg2
  find . -name migrations | xargs rm -r
  ./hue syncdb --migrate --noinput
  EOH
end

service 'postgresql' do
  action [:restart, :enable]
end
