#TODO Run Hue as a service
#TODO Change oozie' password at postgres

HADOOP_USER = node['user']
OOZIE_VERSION = node['releases']['oozie']
TARBALL_URL = "http://ftp.unicamp.br/pub/apache/oozie/#{OOZIE_VERSION}/oozie-#{OOZIE_VERSION}.tar.gz"
OOZIE_DIR = "/usr/local/oozie-#{OOZIE_VERSION}"
OOZIE_TMP_DIR = "/home/#{HADOOP_USER}/oozie-#{OOZIE_VERSION}"
BUILT_OOZIE_DIR = "#{OOZIE_TMP_DIR}/distro/target/oozie-#{OOZIE_VERSION}-distro/oozie-#{OOZIE_VERSION}"
OOZIE_DB_USER = 'oozie'
OOZIE_DB_NAME = 'oozie'

package 'zip'
package 'unzip'

tar_extract TARBALL_URL do
  target_dir "/home/#{HADOOP_USER}"
  creates OOZIE_DIR
end

execute 'build oozie' do
  command "cd #{OOZIE_TMP_DIR} && bin/mkdistro.sh -Puber -DskipTests"
  not_if "ls #{OOZIE_DIR}"
end

execute 'move oozie to /usr/local' do
  command "mv #{BUILT_OOZIE_DIR} /usr/local"
  not_if "ls #{OOZIE_DIR}"
end

directory OOZIE_TMP_DIR do
  recursive true
  action :delete
end

execute 'change permission of oozie folder' do
  command "chown -R #{HADOOP_USER}: #{OOZIE_DIR}"
end

#FIXME Is this really needed?
#execute 'move share_lib to hdfs' do
#  cwd OOZIE_DIR
#  command './bin/oozie-setup.sh sharelib create -fs hdfs://namenode:54310 -locallib oozie-sharelib-4.3.0.tar.gz'
#  user HADOOP_USER
#end

template "#{OOZIE_DIR}/conf/oozie-site.xml" do
  mode 0644
  owner HADOOP_USER
  group HADOOP_USER
  variables({
    PROXY_USER: HADOOP_USER
  })
end

template "#{OOZIE_DIR}/conf/hadoop-conf/core-site.xml" do
  cookbook 'node'
  mode 0644
  owner HADOOP_USER
  group HADOOP_USER
end

template "#{OOZIE_DIR}/conf/hadoop-conf/hdfs-site.xml" do
  cookbook 'node'
  mode 0644
  owner HADOOP_USER
  group HADOOP_USER
end

template "#{OOZIE_DIR}/conf/hadoop-conf/yarn-site.xml" do
  cookbook 'node'
  owner HADOOP_USER
  mode 0644
  group HADOOP_USER
end

template "#{OOZIE_DIR}/conf/hadoop-conf/mapred-site.xml" do
  cookbook 'node'
  mode 0644
  owner HADOOP_USER
  group HADOOP_USER
end

execute "creating postgres' user for #{OOZIE_DB_USER}" do
  psql_command = "CREATE USER #{OOZIE_DB_USER} with createdb login password 'oozie'"
  command "psql -U postgres -c #{ '"' + psql_command + '"' }"
  user 'postgres'
  ignore_failure true
end

execute "create database #{OOZIE_DB_NAME}" do
  command "psql -U postgres -c 'CREATE DATABASE #{OOZIE_DB_NAME} OWNER #{OOZIE_DB_USER}'"
  user 'postgres'
  ignore_failure true
end

#FIXME find the correct way of initializing the service
execute 'restart oozie' do
  cwd OOZIE_DIR
  command 'bin/oozied.sh stop && bin/oozied.sh start'
  user HADOOP_USER
  ignore_failure true
end

