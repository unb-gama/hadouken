HADOOP_USER = node['user']
SOLR_VERSION = node['releases']['solr']
SOLR_DIR = "/usr/local/solr-#{SOLR_VERSION}"
SOLR_USER = 'solr'
SOLR_TAR_URL = "http://ftp.unicamp.br/pub/apache/lucene/solr/#{SOLR_VERSION}/solr-#{SOLR_VERSION}.tgz"

execute "create Solr user" do
  command "adduser -M -U #{SOLR_USER}"
  not_if "id -u #{SOLR_USER}"
end

output = `ls #{SOLR_DIR}`
if output.eql? ""
  tar_extract SOLR_TAR_URL do
    target_dir '/usr/local'
  end
end

cookbook_file "#{SOLR_DIR}/server/solr/solr.xml" do
  owner SOLR_USER
  mode "0755"
end

execute "change permissions for #{SOLR_DIR}" do
  command "chown -R #{HADOOP_USER}: #{SOLR_DIR}"
end

