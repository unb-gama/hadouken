HADOOP_USER = node['user']
HADOOP_HOME = "/home/#{HADOOP_USER}"
HADOOP_DIR = "#{HADOOP_HOME}/hadoop-#{node['releases']['hadoop']}"

HIVE_VERSION = node['releases']['hive']
HIVE_DIR = "/usr/local/hive"
HIVE_FULL_PATH = "/usr/local/hive/apache-hive-#{HIVE_VERSION}-bin"
HIVE_TARBALL_URL = "http://ftp.unicamp.br/pub/apache/hive/hive-#{HIVE_VERSION}"\
                   "/apache-hive-#{HIVE_VERSION}-bin.tar.gz"
HIVE_DB_USER = 'hive'
HIVE_DB_NAME = 'metastore'

package 'postgresql-server'
package 'postgresql-jdbc'

directory HIVE_DIR do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

command = "ls #{HIVE_DIR}"
output = `#{command}`
if output.eql? ""
  tar_extract HIVE_TARBALL_URL do
    target_dir HIVE_DIR
  end
end

execute "change permissions for #{HIVE_DIR}" do
  command "chown -R #{HADOOP_USER}: #{HIVE_DIR}"
end

template "#{HADOOP_HOME}/.hiverc" do
  source 'hiverc'
  mode '0755'
  variables({
    HIVE_HOME: HIVE_FULL_PATH,
  })
end

hiverc_path = File.join(HADOOP_HOME, ".hiverc")
bashrc_path = File.join(HADOOP_HOME, ".bashrc")
bootstrap_file = File.join(HADOOP_HOME, ".hive-bootstrap")
bash 'setup .hiverc' do
  code <<-EOF
    cat #{hiverc_path} >> #{bashrc_path}
    touch #{bootstrap_file}
  EOF
  not_if { ::File.exists?(bootstrap_file) }
end

link "#{HIVE_FULL_PATH}/lib/postgresql-jdbc.jar" do
  to '/usr/share/java/postgresql-jdbc.jar'
end

cookbook_file "#{HIVE_FULL_PATH}/conf/hive-site.xml" do
  owner node['user']
  group node['user']
  mode 0755
end

execute "init postgresql cluster" do
  command "postgresql-setup initdb || true"
end

cookbook_file '/var/lib/pgsql/data/postgresql.conf' do
  mode '0600'
  owner 'postgres'
  group 'postgres'
end

cookbook_file '/var/lib/pgsql/data/pg_hba.conf' do
  mode '0600'
  owner 'postgres'
  group 'postgres'
end

service 'postgresql' do
  action [:restart, :enable]
end

# TODO: run initschema

execute "createuser:#{HIVE_DB_USER}" do
  command 'createuser hive'
  user 'postgres'
  only_if do
    `sudo -u postgres -i psql --quiet --tuples-only -c "select count(*) from pg_user where usename = '#{HIVE_DB_USER}';"`.strip.to_i == 0
  end
end

execute "createdb:#{HIVE_DB_NAME}" do
  command "createdb --owner=#{HIVE_DB_USER} #{HIVE_DB_NAME}"
  user 'postgres'
  only_if do
    `sudo -u postgres -i psql --quiet --tuples-only -c "select count(1) from pg_database where datname = '#{HIVE_DB_NAME}';"`.strip.to_i == 0
  end
end

template "/usr/lib/systemd/system/hive.service" do
  source 'hive.service.erb'
  mode '0755'
  variables({
    HADOOP_HOME: HADOOP_DIR,
    HIVE_HOME: HIVE_FULL_PATH,
    HADOOP_USER: HADOOP_USER
  })
end

service 'hive' do
  action [:start, :enable]
end
